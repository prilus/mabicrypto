package mabicrypto

/**
Original Code: https://github.com/regomne/mabi-pack2

MIT License

Copyright (c) 2015 Utkarsh Kukreti

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

type Context struct {
	s [16]uint32
	r [2]uint32
}

type IV [4]uint32

type Key128BitT = [16]byte
type Key256BitT = [32]byte

type KeystreamT = [16]uint32

func (t *Context) loadIV(iv IV) {
	t.s[15] ^= iv[3]
	t.s[12] ^= iv[2]
	t.s[10] ^= iv[1]
	t.s[9] ^= iv[0]
}

func (t *Context) initialClocking() {
	f := func() {
		for i := 0; i < 16; i++ {
			outfrom_fsm := (t.r[0] + t.s[(i+15)%16]) ^ t.r[1]
			t.s[(i+0)%16] = alphaMul(t.s[(i+0)%16]) ^ t.s[(i+2)%16] ^ alphaInvMul(t.s[(i+11)%16]) ^ outfrom_fsm
			fsmtmp := t.r[1] + t.s[(i+5)%16]
			t.r[1] = table0[uint8(t.r[0]>>0)] ^ table1[uint8(t.r[0]>>8)] ^ table2[uint8(t.r[0]>>16)] ^ table3[uint8(t.r[0]>>24)]
			t.r[0] = fsmtmp
		}
	}

	for i := 0; i < 2; i++ {
		f()
	}
}

func (t *Context) Keystream() KeystreamT {
	v := KeystreamT{}

	for i := 0; i < 16; i++ {
		t.s[(0+i)%16] = alphaMul(t.s[(0+i)%16]) ^ t.s[(2+i)%16] ^ alphaInvMul(t.s[(11+i)%16])
		fsmtmp := t.r[1] + t.s[(5+i)%16]
		t.r[1] = table0[uint8(t.r[0]>>0)] ^ table1[uint8(t.r[0]>>8)] ^ table2[uint8(t.r[0]>>16)] ^ table3[uint8(t.r[0]>>24)]
		t.r[0] = fsmtmp
		v[i] = (t.r[0] + t.s[(0+i)%16]) ^ t.r[1] ^ t.s[(1+i)%16]
	}

	return v
}

func (t *Context) loadKey128Bit(key Key128BitT, iv IV) {
	t.s[15] = mabiendianUint32(key[0:4])
	t.s[14] = mabiendianUint32(key[4:8])
	t.s[13] = mabiendianUint32(key[8:12])
	t.s[12] = mabiendianUint32(key[12:16])

	/* bitwise inverse */
	t.s[11] = ^t.s[15]
	t.s[10] = ^t.s[14]
	t.s[9] = ^t.s[13]
	t.s[8] = ^t.s[12]

	/* just copy */
	copy(t.s[0:8], t.s[8:16])

	t.r[0], t.r[1] = 0, 0

	t.loadIV(iv)
	t.initialClocking()
}

func (t *Context) loadKey256Bit(key Key256BitT, iv IV) {
	t.s[15] = mabiendianUint32(key[0:4])
	t.s[14] = mabiendianUint32(key[4:8])
	t.s[13] = mabiendianUint32(key[8:12])
	t.s[12] = mabiendianUint32(key[12:16])
	t.s[11] = mabiendianUint32(key[16:20])
	t.s[10] = mabiendianUint32(key[20:24])
	t.s[9] = mabiendianUint32(key[24:28])
	t.s[8] = mabiendianUint32(key[28:32])

	/* bitwise inverse */
	t.s[7] = ^t.s[15]
	t.s[6] = ^t.s[14]
	t.s[5] = ^t.s[13]
	t.s[4] = ^t.s[12]
	t.s[3] = ^t.s[11]
	t.s[2] = ^t.s[10]
	t.s[1] = ^t.s[9]
	t.s[0] = ^t.s[8]

	t.r[0], t.r[1] = 0, 0

	t.loadIV(iv)
	t.initialClocking()
}

func New128BitStream(key Key128BitT, iv IV) *Context {
	v := new(Context)

	v.loadKey128Bit(key, iv)

	return v
}

func New256BitStream(key Key256BitT, iv IV) *Context {
	v := new(Context)

	v.loadKey256Bit(key, iv)

	return v
}

func alphaMul(w uint32) uint32 {
	return (w << 8) ^ (alphaMulTable[uint8(w>>24)])
}

func alphaInvMul(w uint32) uint32 {
	return (w >> 8) ^ (alphaInvMulTable[uint8(w)])
}

func mabiendianUint32(a []byte) uint32 {
	v := uint32(int8(a[0]))<<24 |
		uint32(int8(a[1]))<<16 |
		uint32(int8(a[2]))<<8 |
		uint32(int8(a[3]))

	return v
}
